#!/bin/bash

mkdir -p ./../root-build

find ./source -name "*.css" | sort | xargs cat > ./../root-build/root.css

mkdir -p ./../root-custom

find ./../root-custom -name "*.css" | sort | xargs cat >> ./../root-build/root.css

cp -R -f ./fonts ./../root-build/fonts
